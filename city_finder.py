import json
from spacy.lang.fr import French
from spacy.tokens import Doc

with open("villes_test.json") as f:
    DATA = json.loads(f.read())

nlp = French()

# Register the Doc extension "author" (default None)
Doc.set_extension("duree", default=None)

# Register the Doc extension "book" (default None)
Doc.set_extension("trajet", default=None)

for doc, context in nlp.pipe(DATA, as_tuples=True):
    # Set the doc._.book and doc._.author attributes from the context
    doc._.duree = context["duree"]
    doc._.trajet = context["trajet"]

    # Print the text and custom attribute data
    print(f"{doc.text}\n — '{doc._.duree}' by {doc._.trajet}\n")