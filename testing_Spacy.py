import spacy
import fr_core_news_md #import du model medium, car utilisable pour training

#on detecte les tokens présents dans notre fichier


def my_component(doc):

    print("Après tokenisation, notre document comporte {} tokens.".format(len(doc)))
    print("Les part-of-speech tags sont:", [token.pos_ for token in doc])
    if len(doc) < 10:
        print("Le document est trop court.")
    return doc


def train_spacy(data, iterations):
    TRAIN_DATA = data
    nlp = spacy.blank('fr')
    if 'ner' not in nlp.pipe_names:
        ner = nlp.create_pipe('ner')
        nlp.add_pipe(ner, last=True)

nlp = fr_core_news_md.load()
doc = nlp("Try to make me happy. Hello World!")
nlp.add_pipe(my_component, name="print_info", last=True)
print(doc.pipe_names)  # ['tagger', 'parser', 'ner', 'print_info']

