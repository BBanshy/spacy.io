import json
from spacy.matcher import Matcher
from spacy.lang.fr import French

with open("./villes_test.json", encoding="utf8") as f:
    TEXTS = json.loads(f.read())

nlp = French()
matcher = Matcher(nlp.vocab)


pattern1 = [{"LOWER": "trajet"}, {"LOWER": "paris"}]

# Ajoute les motifs au matcher et vérifie le résultat
matcher.add('GADGET', None, pattern1)
for doc in nlp.pipe(TEXTS):
    print([doc[start:end] for match_id, start, end in matcher(doc)])