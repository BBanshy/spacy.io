import json
from spacy.lang.fr import French
from spacy.tokens import Span
from spacy.matcher import PhraseMatcher

# Passer le csv en json pour essayer de matcher
with open("villes_test.json") as f:
    VILLES = json.loads(f.read())

with open("villes_test.json") as f:
    GARES = json.loads(f.read())

nlp = French()
matcher = PhraseMatcher(nlp.vocab)
matcher.add("VILLES", None, *list(nlp.pipe(VILLES)))


def villes_component(doc):
    matches = matcher(doc)
    doc.ents = [Span(doc, start, end, label="GPE") for match_id, start, end in matches]
    return doc


# Ajout du component à une pipeline
nlp.add_pipe(villes_component())
print(nlp.pipe_names)

#Recupère les gares dans la phrase; doc
get_gares = lambda span: GARES.get(span.text)


Span.set_extension("gare", getter=get_gares)

doc = nlp("Le train arrivera en gare de Lyon-Nord")
print([(ent.text, ent.label_, ent._.capital) for ent in doc.ents])

# Ca marche pas c'est le bordel